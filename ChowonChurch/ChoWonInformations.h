//
//  ChoWonInformations.h
//  ChowonChurch
//
//  Created by yoseop on 2014. 9. 29..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChoWonInformations : NSObject

@property (strong) NSString *title;
@property (strong) NSString *author;
@property (strong) NSString *category;
@property (strong) NSString *infoDescription;
@property (strong) NSString *link;
@property (strong) NSString *text;
@property (strong) NSString *pubDate;


@end
