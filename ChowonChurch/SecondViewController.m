//
//  SecondViewController.m
//  ChowonChurch
//
//  Created by yoseop on 2014. 9. 26..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "SecondViewController.h"
#import "SecondCategoryTableViewCell.h"

#import "WebViewController.h"
#import "choWonInformations.h"
#import "HTMLParsingViewController.h"

#import "ContactsTableViewController.h"
#import "RIButtonItem.h"
#import "UIAlertView+Blocks.h"

@interface SecondViewController () <UITableViewDataSource, UITableViewDelegate>
{
    int _touchCount;
}
@property (strong) NSArray *allCategories;
@property (strong) NSArray *firstArray;
@property (strong) NSArray *secondArray;
@property (strong) NSArray *thirdArray;
@property (strong) NSArray *fourthArray;
@property (strong) NSArray *fifthArray;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *choWonHomeItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"home"] style:UIBarButtonItemStylePlain target:self action:@selector(goToChowonDotOrg)];//goToChowonDotOrg
//    choWonHomeItem.image = [choWonHomeItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.rightBarButtonItem = choWonHomeItem;
    
    self.title = @"전체 메뉴";
    [self categoryString];
    _touchCount = 0;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (_touchCount != 10) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"      " style:UIBarButtonItemStylePlain target:self action:@selector(didTapRightButtonItem)];
    }
    if ([self.activityIndicatorView isAnimating]) {
        [self.activityIndicatorView stopAnimating];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if ([self.activityIndicatorView isAnimating]) {
        [self.activityIndicatorView stopAnimating];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)categoryString
{
    self.firstArray = @[@"우리교회는",@"섬기는 분들",@"교회 연혁",@"제직위원회",@"지구 및 구역",@"오시는길",@"초원의 꿈",@"초원동산",@"기적의 교회",@"교회행사포토",@"온라인헌금"];
    self.secondArray = @[@"초원션교현황",@"선교지에서온편지",@"지역사랑학교",@"NEPALMISSION"];
    self.thirdArray = @[@"열린게시판",@"교회일정표",@"초원글마당",@"교우사업소개",@"김명조법률상담",@"교회소식",@"간증나눔",@"교회행사동영상",@"새가족포토",@"건강상식코너",@"초원UCC",@"행복나눔포토",@"그린큐티"];
    self.fourthArray = @[@"영어성경듣기",@"신구약66권개요",@"초원제직훈련",@"초원전도폭발",@"초원세례문답공부",@"영어성경자료실"];
    self.fifthArray = @[@"목회칼럼", @"성경과 신앙",@"서식"];
    
    self.allCategories = @[self.firstArray,self.secondArray,self.thirdArray,self.fourthArray,self.fifthArray];
    
    
}

#pragma mark - TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.allCategories.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ((NSArray *)self.allCategories[section]).count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SecondCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SecondCategoryTableViewCell"];
    /*
        @"<초원교회>"
        @"<초원선교현황>"
        @"<초원사랑방>"
        @"<초원두란노>"
        @"<말씀과 목회칼럼>"
     */
    
    NSArray *sectionDatas = self.allCategories[indexPath.section];
    cell.titleLabel.text = sectionDatas[indexPath.row];
    [cell.titleLabel adjustsFontSizeToFitWidth];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    ChoWonInformations *choWonInformations = [ChoWonInformations new];
    choWonInformations.link = [self uRLLinkForWebViewWithIndexPath:indexPath];
    webViewController.choWonInformations = choWonInformations;
    webViewController.webViewType = WebViewTypeWithURL;
    NSArray *sectionDatas = self.allCategories[indexPath.section];
    choWonInformations.title = sectionDatas[indexPath.row];

    [self.navigationController pushViewController:webViewController animated:YES];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSArray *array = @[@"초원교회",@"초원선교현황",@"초원사랑방",@"초원두란노",@"말씀과 목회칼럼"];
    return array[section];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
//    UIImage *myImage = [UIImage imageNamed:@"cell8"];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:myImage];
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,30)];
    
    NSArray *array = @[@"초원교회",@"초원선교현황",@"초원사랑방",@"초원두란노",@"말씀과 목회칼럼"];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20,0,[UIScreen mainScreen].bounds.size.width,50)];
    label.textAlignment = NSTextAlignmentLeft;
    label.text = array[section];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textColor = [UIColor darkGrayColor];
    
//    CGFloat colorValue1 = 220.0/255.0;
//    CGFloat colorValue2 = 220.0/255.0;
//    CGFloat colorValue3 = 220.0/255.0;
//    UIColor *backGroundColor = [[UIColor alloc] initWithRed:colorValue1 green:colorValue2 blue:colorValue3 alpha:1];
//    titleView.backgroundColor = backGroundColor;
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"titleBackground"]];
    [imgView addSubview:label];
    [titleView addSubview:imgView];
    
    return titleView;
    
}
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    UIImage *myImage = [UIImage imageNamed:@"cell10"];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:myImage];
//    imageView.frame = CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,5);
//    return imageView;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (NSString *)uRLLinkForWebViewWithIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
            {
//                @"우리교회는", (2, 0)
                return [self mobileUrlStringWithMainString:@"LayoutView.aspx?" module_seq:2 moduleNum:30];
            }
                break;
            case 1:
            {
//                @"섬기는 분들", (152, 0)
                return [self mobileUrlStringWithMainString:@"LayoutView.aspx?" module_seq:152 moduleNum:30];
            }
                break;
            case 2:
            {
//                @"교회 연혁", (182, 0)
                return [self mobileUrlStringWithMainString:@"LayoutView.aspx?" module_seq:182 moduleNum:30];
            }
                break;
            case 3:
            {
//                @"제직위원회", 57
                return [self mobileUrlStringWithMainString:@"LayoutView.aspx?" module_seq:57 moduleNum:30];
            }
                break;
            case 4:
            {
//                @"지구 및 구역",53
                return [self mobileUrlStringWithMainString:@"LayoutView.aspx?" module_seq:53 moduleNum:30];
            }
                break;
            case 5:
            {
//                @"오시는길", 6
                return [self mobileUrlStringWithMainString:@"LayoutView.aspx?" module_seq:6 moduleNum:30];
            }
                break;
            case 6:
            {
//                @"초원의 꿈", 55
                return [self mobileUrlStringWithMainString:@"LayoutView.aspx?" module_seq:55 moduleNum:30];
            }
                break;
            case 7:
            {
//                @"초원동산", 56
                return [self mobileUrlStringWithMainString:@"LayoutView.aspx?" module_seq:56 moduleNum:30];
            }
                break;
            case 8:
            {
//                @"기적의 교회", 114
                return [self mobileUrlStringWithMainString:@"LayoutView.aspx?" module_seq:114 moduleNum:30];
            }
                break;
            case 9:
            {
//                @"교회행사포토", 88
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:88 moduleNum:6];
            }
                break;
            case 10:
            {
//                @"온라인현금" 129
                return [self mobileUrlStringWithMainString:@"LayoutView.aspx?" module_seq:129 moduleNum:30];
            }
                break;
                
                
            default:
                break;
        }
    }else if (indexPath.section == 1) {
        switch (indexPath.row) {
            case 0:
            {
//                @"초원선교현황", 181,43
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:181 moduleNum:1];
            }
                break;
            case 1:
            {
//                @"선교지에서온편지", 61,43
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:61 moduleNum:1];
                
            }
                break;
            case 2:
            {
//                @"지역사랑학교", 63,43
                return [self mobileUrlStringWithMainString:@"LayoutView.aspx?" module_seq:63 moduleNum:30];

            }
                break;
            case 3:
            {
//                @"NEPALMISSION" 64,43
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:64 moduleNum:1];
            }
                break;
                
            default:
                break;
        }
    }else if (indexPath.section == 2) {
        switch (indexPath.row) {
            case 0:
            {
//                @"열린게시판", 90, 13
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:90 moduleNum:1];
            }
                break;
            case 1:
            {
                //@"교회일정표"
                return @"http://chowon6420.cafe24.com/zeroboard/zboard.php?id=schedule";
            }
                break;
            case 2:
            {
//                @"초원글마당", 115
//                return [self urlStringWithmodule_seq:115 menu_order:13];
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:115 moduleNum:1];
            }
                break;
            case 3:
            {
//                @"교우사업소개", 147
//                return [self urlStringWithmodule_seq:147 menu_order:13];
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:147 moduleNum:1];
            }
                break;
            case 4:
            {
//                @"김명조법률상담"
                return @"http://php.chol.com/~mj0426/technote/board.php?board=law2";

            }
                break;
            case 5:
            {
//                @"교회소식", 51
//                return [self urlStringWithmodule_seq:51 menu_order:13];
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:51 moduleNum:1];
            }
                break;
            case 6:
            {
//                @"간증나눔", 22
//                return [self urlStringWithmodule_seq:22 menu_order:13];
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:22 moduleNum:1];

            }
                break;
            case 7:
            {
//                @"교회행사동영상", 66
                http://mhompy.onmam.com/BbsList.aspx?ModuleSeq=66&UserNum=0&HompyNum=62624&ModuleNum=1
//                return [self urlStringWithmodule_seq:66 menu_order:13];
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:66 moduleNum:1];
                
            }
                break;
            case 8:
            {
//                @"새가족포토", 89 6
//                return [self urlStringWithmodule_seq:89 menu_order:13];
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:89 moduleNum:6];
            }
                break;
            case 9:
            {
//                @"건강상식코너", 117
//                return [self urlStringWithmodule_seq:117 menu_order:13];
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:117 moduleNum:1];
            }
                break;
            case 10:
            {
//                @"초원UCC", 170
//                return [self urlStringWithmodule_seq:170 menu_order:13];
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:170 moduleNum:1];
            }
                break;
            case 11:
            {
//                @"행복나눔포토" 84 6
//                return [self urlStringWithmodule_seq:84 menu_order:13];
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:84 moduleNum:6];
            }
                break;
            case 12:
            {
                //                @"그린큐티" 116
                //                return [self urlStringWithmodule_seq:116 menu_order:13];
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:116 moduleNum:1];
            }
                break;
                
                
            default:
                break;
        }
    }else if (indexPath.section == 3) {
        switch (indexPath.row) {
            case 0:
            {
//                @"영어성경듣기", 174, 35
                return [self mobileUrlStringWithMainString:@"LayoutView.aspx?" module_seq:174 moduleNum:30];
            }
                break;
            case 1:
            {
//                @"신구약66권개요", 29, 35
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:29 moduleNum:1];
            }
                break;
            case 2:
            {
//                @"초원제직훈련", 121, 35
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:121 moduleNum:1];
            }
                break;
            case 3:
            {
//                @"초원전도폭발", 118, 35
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:118 moduleNum:1];

            }
                break;
            case 4:
            {
//                @"초원세례문답공부", 120, 35
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:120 moduleNum:1];
            }
                break;
            case 5:
            {
//                @"영어성경자료실", 30, 35
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:30 moduleNum:1];
            }
                break;
            default:
                break;
        }
    }else{
        switch (indexPath.row) {
            case 0:
            {
//                @"목회칼럼", 32, 28
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:112 moduleNum:1];
            }
                break;
            case 1:
            {
//                @"성경과 신앙", 32, 28
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:32 moduleNum:1];
            }
                break;
            case 2:
            {
//                @"서식" 127, 28
                return [self mobileUrlStringWithMainString:@"BbsList.aspx?" module_seq:127 moduleNum:1];
            }
                break;

            default:
                break;
        }
    }
    return @"http://naver.com";
}

- (NSString *)webUrlStringWithmodule_seq:(int)module_seq menu_order:(int)menu_order
{
    return [NSString stringWithFormat:@"http://hompy.onmam.com/Hompi/HomLayoutView.aspx?hompi_num=62624&module_seq=%d&menu_order=%d",module_seq,menu_order];
}

- (NSString *)mobileUrlStringWithMainString:(NSString *)string module_seq:(int)module_seq moduleNum:(int)moduleNum
{
    NSString *baseURL = @"http://mhompy.onmam.com/";
    baseURL = [baseURL stringByAppendingString:string];
    baseURL = [baseURL stringByAppendingFormat:@"ModuleSeq=%d&UserNum=0&HompyNum=62624&ModuleNum=%d",module_seq, moduleNum];
    return baseURL;
}
- (void)didTapRightButtonItem
{
    ++_touchCount;
    
    if (_touchCount == 10) {
        _touchCount++;
        RIButtonItem *exitButtonItem = [RIButtonItem itemWithLabel:@"확인"];
        [exitButtonItem setAction:^{

            UIBarButtonItem *contactItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"contacts"] style:UIBarButtonItemStylePlain target:self action:@selector(goToContactView)];
//            contactItem.image = [contactItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            self.navigationItem.leftBarButtonItem = contactItem;
            
        }];
        UIAlertView *netWorkErrorAlertView = [[UIAlertView alloc] initWithTitle:@"초원 교회" message:@"사랑과 평화가 넘치는 초원교회에 오신것을 \n환영합니다." cancelButtonItem:nil otherButtonItems:exitButtonItem, nil];
        [netWorkErrorAlertView show];
    }
    else if(_touchCount > 10) {
        _touchCount = 0;
    }else{
        return;
    }
}

- (void)goToContactView
{
    ContactsTableViewController *contactsTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactsTableViewController"];
    contactsTableViewController.allContacts = [self loadContacts];
    [self.navigationController pushViewController:contactsTableViewController animated:YES];
    
}

#pragma mark - 연락처 정보 가져오기.

- (NSArray *)loadContacts
{
    [self.activityIndicatorView startAnimating];
    
    NSString *chowonContactsPath = [[NSBundle mainBundle] pathForResource:@"chowonContacts" ofType:@"json"];
    NSError *error = nil;
    NSData *chowonContactData = [NSData dataWithContentsOfFile:chowonContactsPath];
    NSDictionary *chowonContactArray = [NSJSONSerialization JSONObjectWithData:chowonContactData
                                                                       options:0 error:&error];
    if (error) {
        NSLog(@"Error : \n%@", error);
    }
    
    NSArray *datas = chowonContactArray[@"allContacts"];
    return [[NSSet setWithArray:datas] allObjects];
}

- (void)goToChowonDotOrg
{
    RIButtonItem *exitButtonItem = [RIButtonItem itemWithLabel:@"확인"];
    [exitButtonItem setAction:^{
        //    NSURL *url = [NSURL URLWithString:@"http://mhompy.onmam.com/Index.aspx?hompinum=62624"];
        NSURL *url = [NSURL URLWithString:@"http://hompy.onmam.com/Hompi/HomMain.aspx?Hompi_num=62624"];
        
        if (![[UIApplication sharedApplication] openURL:url]) {
            NSLog(@"%@%@",@"Failed to open url:",[url description]);
        }
    }];
    RIButtonItem *cancelButtonItem = [RIButtonItem itemWithLabel:@"취소"];
    UIAlertView *netWorkErrorAlertView = [[UIAlertView alloc] initWithTitle:@"초원 교회" message:@"PC 버전으로 이동하시겠습니까?" cancelButtonItem:cancelButtonItem otherButtonItems:exitButtonItem, nil];
    [netWorkErrorAlertView show];
}

@end
