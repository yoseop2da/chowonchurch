//
//  ContactsTableViewController.h
//  ChowonChurch
//
//  Created by yoseop on 2014. 10. 1..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsTableViewController : UITableViewController

@property (strong) NSArray *allContacts; // 실제로 사용할 데이터

@end
