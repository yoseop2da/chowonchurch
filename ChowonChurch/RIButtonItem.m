//
//  RIButtonItem.m
//  ChowonChurch
//
//  Created by yoseop on 2014. 10. 1..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "RIButtonItem.h"

@implementation RIButtonItem
@synthesize label;
@synthesize action;

+(id)item
{
    return [self new];
}

+(id)itemWithLabel:(NSString *)inLabel
{
    id newItem = [self item];
    [newItem setLabel:inLabel];
    return newItem;
}

@end
