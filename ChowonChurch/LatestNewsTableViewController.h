//
//  LatestNewsTableViewController.h
//  ChowonChurch
//
//  Created by yoseop on 2014. 9. 29..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LatestNewsTableViewController : UITableViewController

@property (strong) NSArray *dataArray;

@end
