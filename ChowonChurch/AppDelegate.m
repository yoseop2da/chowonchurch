//
//  AppDelegate.m
//  ChowonChurch
//
//  Created by yoseop on 2014. 9. 26..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    UIColor *color = [UIColor colorWithRed:60/255.0 green:29/255.0 blue:28/255.0 alpha:1.0f];
    UIColor *textColor = [UIColor colorWithRed:247/255.0 green:234/255.0 blue:0/255.0 alpha:1.0f];
    textColor = [UIColor whiteColor];

    
    // 네비게이션 컬러
    [[UINavigationBar appearance] setBarTintColor:color];
    
    // 타이틀 컬러
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: textColor}];
    
    // 바버튼 아이템 컬러
    [[UINavigationBar appearance] setTintColor:textColor];

//    // 텝바 컬러
//    [[UITabBar appearance] setBarTintColor:color];
//    
    // 텝바 아이템 컬러
    [[UITabBar appearance] setTintColor:color];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
