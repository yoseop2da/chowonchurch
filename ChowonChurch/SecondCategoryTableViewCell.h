//
//  SecondCategoryTableViewCell.h
//  ChowonChurch
//
//  Created by 박요섭 on 2014. 9. 29..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondCategoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
