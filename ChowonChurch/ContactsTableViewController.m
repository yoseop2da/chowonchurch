//
//  ContactsTableViewController.m
//  ChowonChurch
//
//  Created by yoseop on 2014. 10. 1..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "ContactsTableViewController.h"
#import "ContactsTableViewCell.h"
#import "RIButtonItem.h"
#import "UIAlertView+Blocks.h"
#import <MessageUI/MessageUI.h>

@interface ContactsTableViewController ()<UISearchBarDelegate,UISearchControllerDelegate, MFMessageComposeViewControllerDelegate>
{
    BOOL _isSearching;
}
@property (strong) NSMutableArray *consonantSectionData; //자음 sectionData
@property (strong) NSArray *indexeArray; //ㄱ-ㅎ까지 인덱스를 저장할 배열
@property (strong) NSMutableArray *searchResult; //검색의 결과를 저장할 배열

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation ContactsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initialize
{
    self.title = @"초원교회 연락망";
    self.consonantSectionData = [NSMutableArray new];
    self.searchResult = [NSMutableArray new];
    
    [self tableviewDataSetting];

}

- (void)tableviewDataSetting
{   
    // 섹션별 테이블뷰 데이터 설정
    self.indexeArray = @[@"ㄱ",@"ㄴ", @"ㄷ", @"ㄹ", @"ㅁ", @"ㅂ", @"ㅅ", @"ㅇ", @"ㅈ", @"ㅊ", @"ㅋ",@"ㅌ", @"ㅍ", @"ㅎ"];
    [self consnantSectionDataSetting];
}

- (void)consnantSectionDataSetting{
    
    //자음별로 각각 배열 생성.
    NSMutableArray *temp[self.indexeArray.count];
    
    for(int i=0 ; i < 14 ; i++){ temp[i] = [NSMutableArray new]; }
    
    for(int i=0 ; i < self.indexeArray.count ; i++)
    {
        NSString * firstStr = self.indexeArray[i];
        
        for(int j=0 ; j < self.allContacts.count ; j++)
        {
            NSDictionary *person = self.allContacts[j];
            if([firstStr isEqualToString:[self subtract:person[@"name"]]])
                [temp[i] addObject:person];
        }
    }
    
    for(int i=0;i<self.indexeArray.count;i++)
    {
        NSString *index = self.indexeArray[i];
        NSArray *people = temp[i];
        if (temp[i].count !=0) {
            NSDictionary *dic = @{@"indexName": index, @"people": people};
            [self.consonantSectionData addObject:dic];
        }
        
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(_isSearching){
        return 1;
    }
    
    else{
        return self.consonantSectionData.count;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_isSearching){
        return self.searchResult.count;
    }
    
    else{
        NSDictionary * dic = self.consonantSectionData[section];
        NSMutableArray * temp = [dic objectForKey:@"people"];
        
        return [temp count];
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(_isSearching){
        return @"검색결과";
    }
    
    else{
        NSDictionary * dic = self.consonantSectionData[section];
        return dic[@"indexName"];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ContactsTableViewCell"];
    
    if(_isSearching){
        NSDictionary *people = self.searchResult[indexPath.row];
        cell.nameLabel.text = people[@"name"];
        cell.phoneNumberLabel.text = people[@"phone"];
    }
    
    else{
        NSDictionary *dic = self.consonantSectionData[indexPath.section];
        NSArray *people = dic[@"people"];
        NSDictionary *person = people[indexPath.row];
        cell.nameLabel.text = person[@"name"];
        cell.phoneNumberLabel.text = person[@"phone"];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *person;
    if (_isSearching) {
        person = self.searchResult[indexPath.row];
    }
    else{
        NSDictionary *dic = self.consonantSectionData[indexPath.section];
        NSArray *people = dic[@"people"];
        person = people[indexPath.row];
    }
    
    //    telprompt: 물어보고 전화
    //    tel: 바로 전화
    
    NSString *phoneCallSchemeString = [@"telprompt://" stringByAppendingString:person[@"phone"]];
    
    RIButtonItem *cancelButtonItem = [RIButtonItem itemWithLabel:@"취소"];
    [cancelButtonItem setAction:^{
    }];
    
    RIButtonItem *callButtonItem = [RIButtonItem itemWithLabel:@"전화하기"];
    [callButtonItem setAction:^{
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:phoneCallSchemeString]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneCallSchemeString]];
        }
    }];
    RIButtonItem *smsButtonItem = [RIButtonItem itemWithLabel:@"문자보내기"];
    [smsButtonItem setAction:^{

        MFMessageComposeViewController *controller = [MFMessageComposeViewController new];
        if([MFMessageComposeViewController canSendText])
        {
            controller.body = @"안녕하세요 반갑습니다.";
            controller.recipients = @[person[@"phone"]];//[NSArray arrayWithObjects:@"(010)8999-7677", nil];
            controller.messageComposeDelegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }
    }];
    
    
    UIAlertView *netWorkErrorAlertView = [[UIAlertView alloc] initWithTitle:person[@"name"] message:nil cancelButtonItem:cancelButtonItem otherButtonItems:smsButtonItem,callButtonItem, nil];
    [netWorkErrorAlertView show];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    if(result == MessageComposeResultCancelled){

    }else if(result == MessageComposeResultSent){
        
    }else{ //MessageComposeResultFailed

    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
//    NSString *string = [self.indexeArray componentsJoinedByString:@" ● "];
//    NSArray *array = [string componentsSeparatedByString:@" "];
//    return array;;
    return self.indexeArray;
}

-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    for(int i=0;i<self.consonantSectionData.count;i++)
    {
        //sectionData에서 한글 자음을 순서대로 가져와서
        NSDictionary * dic = self.consonantSectionData[i];
        NSString * indexName = dic[@"indexName"];
        
        if([indexName isEqualToString:title])
        {
            return i;
        }
    }
    return -1;
}

#pragma mark - SearchBar Delegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
// return NO to not become first responder
{
    return YES;
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
// return NO to not resign first responder
{
    _isSearching = NO;
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
// called when text changes (including clear)
{
    _isSearching = YES;
    [self.searchResult removeAllObjects];
    if ([searchText intValue]) {
        [self searchByPhoneNumber:searchText];
        
    }else{
        [self searchByName:searchText];
    }
    
    if (searchText.length == 0) {
//        [searchBar resignFirstResponder];
        _isSearching = NO;
    }
    [self.tableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
// called when keyboard search button pressed
{
    [self.searchResult removeAllObjects];
    
    NSString * searchText = searchBar.text;
    
    if ([searchText intValue]) {
        [self searchByPhoneNumber:searchText];
    }else{
        [self searchByName:searchText];
    }
    
    _isSearching = NO;
    [searchBar resignFirstResponder];
    [self.tableView reloadData];
}

#pragma mark - 검색결과 찾기
- (void)searchByPhoneNumber:(NSString *)phoneNumber
{
    for(NSDictionary *person in self.allContacts)
    {
        NSString *name = person[@"phone"];
        NSRange range = [name rangeOfString:phoneNumber];
        if(range.location !=NSNotFound)
        {
            [self.searchResult addObject:person];
        }
    }
}

- (void)searchByName:(NSString *)searchText
{
    for(NSDictionary *person in self.allContacts)
    {
//        // 풀네임
        NSString *name = person[@"name"];
        NSRange range = [name rangeOfString:searchText];
        if(range.location !=NSNotFound)
        {
            [self.searchResult addObject:person];
        }
        
        else{
            
            // 초성으로 검색
            NSString *cosungName = [self GetUTF8String:name];
            NSString *searChTextChosung = searchText;//[self GetUTF8String:searchText];
            
            NSRange cosungNameRange = [cosungName rangeOfString:searChTextChosung];
            
            if(cosungNameRange.location !=NSNotFound)
            {
                [self.searchResult addObject:person];
            }
            
        }
    }
}


#pragma mark - 자음 비교값.
-(NSString *)subtract:(NSString *)str;
{
    int result;
    result = [str compare:@"나"];
    if(result == -1)    return @"ㄱ";
    result = [str compare:@"다"];
    if(result ==-1)    return @"ㄴ";
    result = [str compare:@"라"];
    if(result ==-1)    return @"ㄷ";
    result = [str compare:@"마"];
    if(result ==-1)    return @"ㄹ";
    result = [str compare:@"바"];
    if(result ==-1)    return @"ㅁ";
    result = [str compare:@"사"];
    if(result ==-1)    return @"ㅂ";
    result = [str compare:@"아"];
    if(result ==-1)    return @"ㅅ";
    result = [str compare:@"자"];
    if(result ==-1)    return @"ㅇ";
    result = [str compare:@"차"];
    if(result ==-1)    return @"ㅈ";
    result = [str compare:@"카"];
    if(result ==-1)    return @"ㅊ";
    result = [str compare:@"타"];
    if(result ==-1)    return @"ㅋ";
    result = [str compare:@"파"];
    if(result ==-1)    return @"ㅌ";
    result = [str compare:@"하"];
    if(result ==-1)    return @"ㅍ";
    return @"ㅎ";
}

- (NSString *)GetUTF8String:(NSString *)hanggulString {
    NSArray *chosung = [[NSArray alloc]initWithObjects:@"ㄱ",@"ㄲ",@"ㄴ",@"ㄷ",@"ㄸ",@"ㄹ",@"ㅁ",@"ㅂ",@"ㅃ",@"ㅅ",@"ㅆ",@"ㅇ",@"ㅈ",@"ㅉ",@"ㅊ",@"ㅋ",@"ㅌ",@"ㅍ",@"ㅎ",nil];
    NSArray *jungsung = [[NSArray alloc]initWithObjects:@"ㅏ",@"ㅐ",@"ㅑ",@"ㅒ",@"ㅓ",@"ㅔ",@"ㅕ",@"ㅖ",@"ㅗ",@"ㅘ",@"ㅙ",@"ㅚ",@"ㅛ",@"ㅜ",@"ㅝ",@"ㅞ",@"ㅟ",@"ㅠ",@"ㅡ",@"ㅢ",@"ㅣ",nil];
    NSArray *jongsung = [[NSArray alloc]initWithObjects:@"",@"ㄱ",@"ㄲ",@"ㄳ",@"ㄴ",@"ㄵ",@"ㄶ",@"ㄷ",@"ㄹ",@"ㄺ",@"ㄻ",@"ㄼ",@"ㄽ",@"ㄾ",@"ㄿ",@"ㅀ",@"ㅁ",@"ㅂ",@"ㅄ",@"ㅅ",@"ㅆ",@"ㅇ",@"ㅈ",@"ㅊ",@"ㅋ",@" ㅌ",@"ㅍ",@"ㅎ",nil];
    NSString *chosungResult = @"";
    NSString *textResult = @"";
    for (int i=0;i<[hanggulString length];i++) {
        NSInteger code = [hanggulString characterAtIndex:i];
        if (code >= 44032 && code <= 55203) {
            NSInteger uniCode = code - 44032;
            NSInteger chosungIndex = uniCode / 21 / 28;
            NSInteger jungsungIndex = uniCode % (21 * 28) / 28;
            NSInteger jongsungIndex = uniCode % 28;
            
            // 초성
            chosungResult = [NSString stringWithFormat:@"%@%@", chosungResult, [chosung objectAtIndex:chosungIndex]];
            
            // 이름 전체
            textResult = [NSString stringWithFormat:@"%@%@%@%@", textResult, [chosung objectAtIndex:chosungIndex], [jungsung objectAtIndex:jungsungIndex], [jongsung objectAtIndex:jongsungIndex]];
        }
        
        else{
            NSString *string = [hanggulString substringWithRange:NSMakeRange(i, 1)];
            chosungResult = [NSString stringWithFormat:@"%@%@", chosungResult,string];
        }
    }
    return chosungResult;
}

@end
