//
//  HTMLParsingViewController.m
//  ChowonChurch
//
//  Created by yoseop on 2014. 9. 30..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "HTMLParsingViewController.h"
#import "HTMLNode.h"
#import "HTMLParser.h"

#import "TFHpple.h"

/*
 NSData  * data      = [NSData dataWithContentsOfFile:@"index.html"];
 
 TFHpple * doc       = [[TFHpple alloc] initWithHTMLData:data];
 NSArray * elements  = [doc search:@"//a[@class='sponsor']"];
 
 TFHppleElement * element = [elements objectAtIndex:0];
 [e text];                       // The text inside the HTML element (the content of the first text node)
 [e tagName];                    // "a"
 [e attributes];                 // NSDictionary of href, class, id, etc.
 [e objectForKey:@"href"];       // Easy access to single attribute
 [e firstChildWithTagName:@"b"]; // The first "b" child node
 */
@interface HTMLParsingViewController ()

@end

@implementation HTMLParsingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *urlString = @"http://hompy.onmam.com/Hompi/HomLayoutView.aspx?hompi_num=62624&module_seq=2&menu_order=0";
    NSError *err;
    HTMLParser *parser = [[HTMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:urlString] error:&err];

    HTMLNode *bodyNode = [parser body];
    NSArray *disableNodeList = [bodyNode findChildrenWithAttribute:@"disabled" matchingName:@"disabled" allowPartial:NO];
    NSString *pattern = @"(.*)\\[(.*)\\]";
    NSMutableArray *disableList = [NSMutableArray array];
    for (HTMLNode *disableNode in disableNodeList) {
        NSString *nodeName = [disableNode getAttributeNamed:@"name"];
        if (nodeName) {
            NSString *disabledPropertyName = [nodeName stringByReplacingOccurrencesOfString:pattern withString:@"$2"
                                                                                    options:NSRegularExpressionSearch
                                                                                      range:NSMakeRange(0, nodeName.length)];
            [disableList addObject:disabledPropertyName];
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)test
{
    NSString *urlString = @"http://hompy.onmam.com/Hompi/HomLayoutView.aspx?hompi_num=62624&module_seq=2&menu_order=0";
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
    
    NSMutableString *fullURLstring = [NSMutableString new];
    
    [fullURLstring appendString:@"<html><head><style>body{font-family:'Helvetica Neue'; font-size:15px;}</style></head><body>"];
    TFHpple* doc = [TFHpple hppleWithData:data encoding:nil isXML:NO];
    //    [[TFHpple alloc] initWithHTMLData:data];
    
//    NSArray* items = [doc searchWithXPathQuery:@"//span[@id='ctl00_cphContents_lblContent']//font//b//font"];
    NSArray* items = [doc searchWithXPathQuery:@"//font[@color='navy']"];
    
    TFHppleElement* element = items.firstObject;
    
    for (TFHppleElement *childrenElement  in element.children) {
        if ([[childrenElement tagName] isEqualToString:@"p"]) {
            for (TFHppleElement *childrenElementsChildren in childrenElement.children) {
                NSLog(@"[ %@ ]",childrenElementsChildren);
            }
        }
        
        //            NSLog(@"childrenElement : %@",childrenElement.raw);
        if (childrenElement.raw) {
            
            
            //                NSLog(@"childrenElement.text : %@",childrenElement.text);
            
            [fullURLstring appendString:childrenElement.raw];
        }
    }
    
    [fullURLstring appendString:@"</body></html>"];
    //    NSLog(@"fullURLstring === %@",fullURLstring);

}
@end
