//
//  WebViewController.h
//  ChowonChurch
//
//  Created by 박요섭 on 2014. 9. 29..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ChoWonInformations.h"

typedef NS_ENUM(NSUInteger, WebViewType) {
    WebViewTypeWithHTML,
    WebViewTypeWithURL
};

@interface WebViewController : UIViewController

@property (strong) ChoWonInformations *choWonInformations;
@property (assign) WebViewType webViewType;
@end
