//
//  WebViewController.m
//  ChowonChurch
//
//  Created by 박요섭 on 2014. 9. 29..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.choWonInformations.title;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.activityIndicatorView startAnimating];
    self.activityIndicatorView.hidesWhenStopped = YES;
//    self.webView.userInteractionEnabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidLayoutSubviews
{
    if(self.webViewType == WebViewTypeWithHTML){
        NSString *formattedString = [NSString stringWithFormat:@"<html><head><style>body{font-family:'Helvetica Neue'; font-size:15px;}</style></head><body>%@</body></html>", self.choWonInformations.infoDescription];
        [self.webView loadHTMLString:formattedString baseURL:nil];
    }else{
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",self.choWonInformations.link]]];
        [self.webView loadRequest:request];
        
        [self.webView setScalesPageToFit:YES];
    }

    /*
        NSString *urlString = @"http://hompy.onmam.com/Hompi/HomLayoutView.aspx?hompi_num=62624&module_seq=2&menu_order=0";
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",urlString]]]];
        [self.webView setScalesPageToFit:YES];
     */
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeOther) {
        
        NSString *fullUrl = [NSString stringWithFormat:@"%@",request];
        NSLog(@"fullUrl : %@", fullUrl);
    }
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidStartLoad");
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if ([self.activityIndicatorView isAnimating]) {
        [self.activityIndicatorView stopAnimating];
    }
//    self.webView.userInteractionEnabled = YES;
    NSLog(@"webViewDidFinishLoad");
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"didFailLoadWithError");
}

@end
