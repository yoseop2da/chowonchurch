//
//  LatestNewsTableViewController.m
//  ChowonChurch
//
//  Created by yoseop on 2014. 9. 29..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "LatestNewsTableViewController.h"
#import "ChoWonInformations.h"
#import "WebViewController.h"
#import "InformationTableViewCell.h"

@interface LatestNewsTableViewController ()

@end

@implementation LatestNewsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    InformationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InformationTableViewCell"];
    
    ChoWonInformations *choWonInformations = self.dataArray[indexPath.row];
    
//    choWonInformations.author
//    choWonInformations.category
//    choWonInformations.infoDescription;
//    choWonInformations.link;
//    choWonInformations.text;
//    choWonInformations.title;
    cell.titleLabel.text = choWonInformations.title;
    cell.authorLabel.text = choWonInformations.author;
    cell.pubDateLabel.text = choWonInformations.pubDate;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChoWonInformations *choWonInformations = self.dataArray[indexPath.row];
    
    WebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    webViewController.choWonInformations = choWonInformations;
    webViewController.webViewType = WebViewTypeWithURL;
    
    [self.navigationController pushViewController:webViewController animated:YES];
}
@end
