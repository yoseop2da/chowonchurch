//
//  RIButtonItem.h
//  ChowonChurch
//
//  Created by yoseop on 2014. 10. 1..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RIButtonItem : NSObject
{
    NSString *label;
    void (^action)();
}
@property (retain, nonatomic) NSString *label;
@property (copy, nonatomic) void (^action)();
+(id)item;
+(id)itemWithLabel:(NSString *)inLabel;
@end