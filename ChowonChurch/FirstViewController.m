//
//  FirstViewController.m
//  ChowonChurch
//
//  Created by yoseop on 2014. 9. 26..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "FirstViewController.h"
#import "ChoWonInformations.h"

#import "XMLReader/XMLReader.h"
#import "LatestNewsTableViewController.h"
#import "CategoryTableViewCell.h"

#import "RIButtonItem.h"
#import "UIAlertView+Blocks.h"

@interface FirstViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *_allCategories;
    
    NSMutableArray *_allInfos;

    BOOL _isLoadedRSSData;
    
}

@property (strong) UIActivityIndicatorView *activityIndicatorView;

@end

@implementation FirstViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"최근 항목";
    
    [self rightBarButtonItem];
    [self settingActivityIndicatorView];

    [self initialize];
    
    [self getChowonRSS:^(BOOL success) {
        if (success) {
            [self stopAnimatingActivityIndicatorView];
            _isLoadedRSSData = success;
            [self.tableView reloadData];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initialize
{
    _allCategories = [NSMutableArray array];
    _allInfos = [NSMutableArray array];
}

- (void)rightBarButtonItem
{
    UIBarButtonItem *choWonHomeItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"home"] style:UIBarButtonItemStylePlain target:self action:@selector(goToChowonDotOrg)];//goToChowonDotOrg
//    choWonHomeItem.image = [choWonHomeItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.rightBarButtonItem = choWonHomeItem;
}

- (void)settingActivityIndicatorView
{
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicatorView.color = [UIColor blackColor];
    self.activityIndicatorView.center = self.view.center;
    [self.tableView addSubview:self.activityIndicatorView];
    [self.activityIndicatorView startAnimating];
    self.activityIndicatorView.hidesWhenStopped = YES;
}

- (void)stopAnimatingActivityIndicatorView
{
    if ([self.activityIndicatorView isAnimating]) {
        [self.activityIndicatorView stopAnimating];
    }
}

#pragma mark - TableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return _allCategories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryTableViewCell"];
    
    cell.titleLabel.text = _allCategories[indexPath.row];
    cell.cellImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"cellImage%d",indexPath.row%5]];
    NSString *category = _allCategories[indexPath.row];
    NSArray *array = [self categoryDataWithCategory:category];
    
    cell.badgeLabel.text = [NSString stringWithFormat:@"%d",array.count];
    cell.badgeLabel.layer.masksToBounds = YES;
    cell.badgeLabel.layer.cornerRadius = 10.f;

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!_isLoadedRSSData) {
        return;
    }
    
    LatestNewsTableViewController *latestNewsTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LatestNewsTableViewController"];
    NSString *category = _allCategories[indexPath.row];
    latestNewsTableViewController.dataArray = [self categoryDataWithCategory:category];
    latestNewsTableViewController.title = category;
    
    [self.navigationController pushViewController:latestNewsTableViewController animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int topGuide = 20;
    int bottomGuide = 49;

    return ([UIScreen mainScreen].bounds.size.height - topGuide - bottomGuide)/_allCategories.count;
}

#pragma mark - load RSS Data

- (void)getChowonRSS:(void(^)(BOOL success))completion
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://chowon23.onmam.com/RSS/"]];
    request.HTTPMethod = @"GET";
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        id dic = [XMLReader dictionaryForXMLData:data error:&connectionError][@"rss"][@"channel"];
        
        NSArray *items = dic[@"item"];

        for (NSDictionary *item in items) {
            
            ChoWonInformations *choWonInformations = [ChoWonInformations new];
            choWonInformations.author = [self deleteBlankWithString:item[@"author"][@"text"]];
            choWonInformations.category = [self deleteBlankWithString:item[@"category"][@"text"]];
            NSString *description = [self deleteBlankWithString:item[@"description"][@"text"]];
            
            // 쓰레기파일 변환.
            description = [description stringByReplacingOccurrencesOfString:@"http://file." withString:@"http://hompydata."];
            description = [description stringByReplacingOccurrencesOfString:@"<img src='http://hompydata.onmam.com/ONMAM_CHURCH/user_data/62624/files/BoardFile' name='imgfile1' width=600 class='margin'/><br />" withString:@""];
            
            choWonInformations.infoDescription = description;
            
            NSString *linkString = [self deleteBlankWithString:item[@"link"][@"text"]];
            choWonInformations.link = [self mobileLink:linkString];
            choWonInformations.text = [self deleteBlankWithString:item[@"text"]];
            choWonInformations.title = [self deleteBlankWithString:item[@"title"][@"text"]];
            NSString *string = [self deleteBlankWithString:item[@"pubDate"][@"text"]];
            choWonInformations.pubDate = [string substringToIndex:(string.length -6)];

//            NSLog(@"author : %@",choWonInformations.author);
//            NSLog(@"category : %@",choWonInformations.category);
//            NSLog(@"text : %@",choWonInformations.text);
            
//            NSLog(@"description : %@",choWonInformations.infoDescription);
//            NSLog(@"link : %@",choWonInformations.link);
//            NSLog(@"title : %@",choWonInformations.title);
            
            if ([choWonInformations.category isEqualToString:@"교회행사동영상"]) {
//
            }else{
                [_allCategories addObject:choWonInformations.category];
                [_allInfos addObject:choWonInformations];
            }
        }
        
        //TODO: 카테고리 중복제거.
        _allCategories = [[[NSSet setWithArray:_allCategories] allObjects] mutableCopy];
        completion(YES);
    }];
}

- (NSString *)mobileLink:(NSString *)urlString
{
    NSArray *comp1 = [urlString componentsSeparatedByString:@"?"];
    
    NSString *firstQuery = comp1.firstObject;
    NSArray *arr1 = [firstQuery componentsSeparatedByString:@"/"];
    NSString *compareValue = [arr1.lastObject componentsSeparatedByString:@"."].firstObject;
    
    NSString *query = comp1.lastObject;
    NSArray *queryElements = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    
    for (NSString *element in queryElements) {
        NSArray *keyVal = [element componentsSeparatedByString:@"="];
        if (keyVal.count > 0) {
            NSString *variableKey = [keyVal objectAtIndex:0];
            NSString *value = (keyVal.count == 2) ? [keyVal lastObject] : nil;
            [paramDic setObject:value forKey:variableKey];
        }
    }
#define HomBbsView @"HomBbsView"
#define HomAlbumView @"HomAlbumView"
    
    NSString *linkString;
    if ([compareValue isEqualToString:HomBbsView])
    {
        linkString = [NSString stringWithFormat:@"http://mhompy.onmam.com/bbsDetailview.aspx?ModuleSeq=%@&Seq=%@&hompinum=%@",paramDic[@"module_seq"],paramDic[@"seq"],paramDic[@"hompi_num"]];
    }
    else if([compareValue isEqualToString:HomAlbumView])
    {
        linkString = [NSString stringWithFormat:@"http://mhompy.onmam.com/AlbumbbsDetailview.aspx?ModuleSeq=%@&Seq=%@&&hompinum=%@",paramDic[@"module_seq"],paramDic[@"seq"],paramDic[@"hompi_num"]];
    }
    else
    {
        linkString = urlString;
    }
    return linkString;
}

- (NSArray *)categoryDataWithCategory:(NSString *)categoryName
{
    NSMutableArray *dataArray = [NSMutableArray array];
    for (ChoWonInformations *info in _allInfos) {
        if ([categoryName isEqualToString:info.category]) {
            [dataArray addObject:info];
        }
    }
    return dataArray;
}

- (NSString *)deleteBlankWithString:(NSString *)string
{
    NSString *replacedString;
    replacedString = [string stringByReplacingOccurrencesOfString:@"  " withString:@""];
    replacedString = [replacedString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return replacedString;
}

- (void)goToChowonDotOrg
{
    RIButtonItem *exitButtonItem = [RIButtonItem itemWithLabel:@"확인"];
    [exitButtonItem setAction:^{
        //    NSURL *url = [NSURL URLWithString:@"http://mhompy.onmam.com/Index.aspx?hompinum=62624"];
        NSURL *url = [NSURL URLWithString:@"http://hompy.onmam.com/Hompi/HomMain.aspx?Hompi_num=62624"];
        
        if (![[UIApplication sharedApplication] openURL:url]) {
                NSLog(@"%@%@",@"Failed to open url:",[url description]);
        }
    }];
    RIButtonItem *cancelButtonItem = [RIButtonItem itemWithLabel:@"취소"];
    UIAlertView *netWorkErrorAlertView = [[UIAlertView alloc] initWithTitle:@"초원 교회" message:@"PC 버전으로 이동하시겠습니까?" cancelButtonItem:cancelButtonItem otherButtonItems:exitButtonItem, nil];
    [netWorkErrorAlertView show];
}

@end
