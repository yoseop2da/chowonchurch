//
//  ContactsTableViewCell.h
//  ChowonChurch
//
//  Created by 박요섭 on 2014. 10. 11..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;

@end
